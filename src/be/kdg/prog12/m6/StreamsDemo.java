package be.kdg.prog12.m6;

import java.io.BufferedInputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class StreamsDemo {
    public static void main(String[] args) throws IOException {
        // System.in is some kind of Stream (InputStream)
        // Scanner scanner = new Scanner(System.in);

        // File file = new File("textfile.txt");
        Path path = Paths.get("textfile.txt");
        if (!Files.exists(path)) {
            Files.createFile(path);
        }

        // FileInputStream --> a binary stream that reads from a file
        FileInputStream fis = new FileInputStream(path.toFile()); // String is OK as well

        int b = fis.read();
        while (b != -1) {
            System.out.println("Byte: " + b);
            b = fis.read();
        }
        System.out.println("EOF reached");

        // DataInputStream --> convenient binary stream for reading binary data
        //    Expects ANY kind of InputStream
        DataInputStream dis = new DataInputStream(new FileInputStream(path.toFile()));
        int i = dis.readInt();
        char c = dis.readChar();
        System.out.println("INT: " + i);
        System.out.println("CHAR: " + c);

        // BufferedInputStream --> Introduces buffering
        //    Expects ANY kind of InputStream
        BufferedInputStream bis = new BufferedInputStream(new FileInputStream(path.toFile()));
        DataInputStream dis2 = new DataInputStream(bis);

        dis2.readInt(); // Reads AT LEAST 4 bytes
        dis2.readInt(); // If the buffer contains data, then no 'disk' access is going to be needed
        dis2.readInt();
        dis2.readInt();


        // Reading BINARY data
        DataInputStream dis3 = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(path.toFile())));
        try {
            // Do our stuff
            String someText = dis3.readUTF(); // Could fail

            // Same with ResultSet, Statement, PreparedStatement
        } catch (IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        } finally {
            dis3.close(); // Important ALWAYS close streams
        }

        // try-with-resources
        // Use ANY AutoCloseable in between '(' and ')'
        try (DataInputStream dis4 = new DataInputStream(
                new BufferedInputStream(
                        new FileInputStream(path.toFile())))) {
            // do stuff
            String someText = dis3.readUTF(); // Could fail
        } catch (IOException e) {
            e.printStackTrace();
            //throw new RuntimeException(e);
        }

        System.out.println("Some more code");
        System.out.println("Some more code");
        System.out.println("Some more code");
    }
}
