package be.kdg.prog12.m6;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

public class Demo {
    public static void main(String[] args) {
        // Regular class, objects can be created through the constructor
        // Relatively 'old' API, but still very relevant today.
        File file = new File("/home/lars/tmp/textfile.txt");
        // Path is an interface. Objects are created using the helper class
        // 'Paths'. (Factory)
        // New API. Can be used with 'Files' as well.
        Path path = Paths.get("/home/lars/tmp/textfile.txt");

        // Files class is a utility class to work with files.
        // Contains static utility methods. Don't create an instance of 'Files'
        if (Files.exists(path)) {
            System.out.println("It exists");
        } else {
            System.out.println("It doesn't exist");
        }

        if (!Files.exists(path)) {
            try {
                Files.createFile(path);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        if (Files.exists(path) && Files.isRegularFile(path)) {
            System.out.println("It exists and is a regular File");
        } else {
            System.out.println("It doesn't exist");
        }

        try {
            List<String> lines = Files.readAllLines(path);

            for (String line : lines) {
                System.out.println("LINE: " + line);
            }
        } catch (IOException e) {
            System.out.println("Can't read file.");
        }

        Path otherPath = Paths.get("/home/lars/tmp/some_copy.txt");
        if (!Files.exists(otherPath)) {
            try {
                Files.copy(path, otherPath);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        try {
            List<String> lines = List.of("First line", "Second line");
            Files.write(otherPath, lines);
        } catch (IOException e) {
            e.printStackTrace();
        }

        Path thirdPath = Paths.get("third.txt");
        // Same as
        // Path thirdPath = Paths.get("./third.txt");
        try {
            Files.createFile(thirdPath);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
